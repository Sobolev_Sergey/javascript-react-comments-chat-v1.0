# JavaScript REACT comments

Приложение "react-comments" выполнено на react. 
1.БАЗА ДАННЫХ. 
Развернута на Firebase облачной NoSQL БД для real-time приложений, которая предоставляет API, по адресу https://test-6b7de.firebaseio.com/ 
Выдает API ответ по адресу https://test-6b7de.firebaseio.com/comments.json В ответ предоставляет comments.json, 
следующего содержания-формата: 
{ 
	"id записи- автоматически генерируется базой" 
	{ 
		"name":"имя коментатора", 
		"text":"текст сообщения" 
	} 
}

ПРИМЕР:

{
	"-Kz9_VzQ6nU5CqKedQkY":
		{
			"name":"Сергей",
			"text":"Привет мир!!!"
		},
	"-Kz9__mdFFEAQB3Yysv9":
		{
			"name":"Сергей",
			"text":"Всем привет!!!"
		}
}

{ "-Kz9_VzQ6nU5CqKedQkY": { "name":"Сергей", "text":"Привет мир!!!" }, "-Kz9__mdFFEAQB3Yysv9": { "name":"Сергей", "text":"Всем привет!!!" } }

ФУНКЦИОНАЛ.
добавление / удаление комментария
добавление Имени автора комментария
отображение комментариев
скрыть / показать комментарии
счетчик колличества комментариев

![Alt text](https://bytebucket.org/Sobolev_Sergey/javascript-react-comments-chat-v1.0/raw/e7c9cf0967a8ffe315e486101bb25964447e7d42/commens.png)
