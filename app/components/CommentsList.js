import React from 'react';
import Comment from "./Comment";
import CommentsTitle from "./CommentsTitle";
import CommentsToggle from "./CommentsToggle";
import ReactMixin from 'react-mixin';
import ReactFireMixin from 'reactfire';
import firebase from 'firebase'﻿;


export default class CommentsList extends React.Component {
// class CommentsList extends React.Component {
    constructor() {
        super();

        this.state = {
            showComments: true,
            comments: []
        }
    }

    componentDidMount() {
        this.bindAsArray(
            firebase.database().ref().child('comments'),   // откуда
            'comments');                                   // куда
    }

    _toggleShowComments() {

        this.setState({
            showComments: !this.state.showComments
        })
    }

    render() {
        // const comments = [
        //     {
        //         name:'Сергей',
        //         text:'Привет всем!'
        //     },
        //     {
        //         name:'Сергей',
        //         text:'всем привет!'
        //     },
        //     {
        //         name:'Сергей',
        //         text:'Hello !'
        //     }
        // ];

        const commentsCount = this.state.comments.length;
        let commentsList;

        if (commentsCount > 0 && this.state.showComments) {
            commentsList =  <ul className="comments-list">
                {
                    this.state.comments.map((comment, index) => {
                        return <Comment key={index}
                                        author={comment.name}
                                        text={comment.text}
                                        id={comment['.key']} />
                    })
                }
            </ul>
        }

        return (
            <div className="comments-body">
                <CommentsTitle counter={commentsCount} />
                <CommentsToggle
                    toggleComments={this._toggleShowComments.bind(this)}
                    isShow={this.state.showComments} />
                {commentsList}
            </div>
        )
    }
}

//module.exports = CommentsList;  // или другой способ
// export default CommentsList;

ReactMixin(CommentsList.prototype, ReactFireMixin);