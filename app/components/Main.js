import React from 'react';
import ReactDOM from 'react-dom';
import CommentsList from './CommentsList'
import CommentsForm from './CommentsForm';
import firebase from 'firebase'﻿;

var config = {
    apiKey: 'AIzaSyCKiz-sKHMt4yW8mqIE_5IbyphFs8Na_yM',
    databaseURL: 'https://test-6b7de.firebaseio.com/'
}

firebase.initializeApp(config);

class MainComponent extends React.Component {
    render() {
        //const currentTime = new Date();
        //const topicList = ['HTML', 'CSS', 'JS', 'React'];

        return (
            /*
            <div>
                <div>I'm a component  {currentTime.toTimeString()}</div>
                <ul>
                    {topicList.map(item => <li>{item}</li>)}
                </ul>
            </div>
            */
            <div className="comments-box">
                <CommentsForm />
                <CommentsList />
            </div>
        )
    }
}

ReactDOM.render(
    <MainComponent/>,
    document.getElementById('app')
)